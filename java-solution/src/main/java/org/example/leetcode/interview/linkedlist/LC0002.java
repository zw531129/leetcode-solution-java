package org.example.leetcode.interview.linkedlist;

import org.example.util.IO;
import org.example.util.ListNode;

public class LC0002 {
    /**
     * 题目答案给的是从低位到高位, 不需要翻转.
     * 遍历两个链表, 列竖式计算出当前位的值(求余), 以及是否需要进位(除, 进位则除出来是1)
     * 用头结点会方便最后返回
     *
     * @param l1
     * @param l2
     * @return
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode(-1);
        ListNode curr = dummy;
        int carry = 0;

        while (l1 != null || l2 != null) {
            int v1 = l1 == null ? 0 : l1.val;
            int v2 = l2 == null ? 0 : l2.val;

            int sum = v1 + v2 + carry;
            // 是否进位
            carry = sum / 10;
            // 本位上的数是多少
            sum = sum % 10;
            curr.next = new ListNode(sum);

            curr = curr.next;
            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;
        }

        // 最高位是否有进一操作
        if (carry == 1) curr.next = new ListNode(carry);

        return dummy.next;
    }

    public static void main(String[] args) {
        ListNode l1 = IO.intLinkedList("[2,4,3]");
        ListNode l2 = IO.intLinkedList("[5,6,4]");
        LC0002 solu = new LC0002();
        solu.addTwoNumbers(l1, l2);
    }
}
