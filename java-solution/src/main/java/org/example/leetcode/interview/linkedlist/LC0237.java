package org.example.leetcode.interview.linkedlist;

import org.example.util.ListNode;

public class LC0237 {
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
