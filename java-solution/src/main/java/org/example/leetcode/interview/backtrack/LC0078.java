package org.example.leetcode.interview.backtrack;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class LC0078 {
    List<List<Integer>> ans = new LinkedList<>();
    Deque<Integer> condition = new LinkedList<>();

    public List<List<Integer>> subsets(int[] nums) {
        if (nums == null || nums.length == 0) return ans;
        backtracking(nums, 0);

        return ans;
    }

    private void backtracking(int[] nums, int startIndex) {
        ans.add(new LinkedList<>(condition));

        if (startIndex >= nums.length) {
            return ;
        }
        for (int i = startIndex; i < nums.length; i++) {
            condition.add(nums[i]);
            backtracking(nums, i + 1);
            condition.removeLast();
        }
    }
}
