package org.example.leetcode.interview.math;

public class LC0169 {
    public int majorityElement(int[] nums) {
        int cand_num = nums[0];
        int count = 1;

        for (int i = 0; i < nums.length; i++) {
            if (cand_num == nums[i]) {
                count++;
            } else if (--count == 0) {
                cand_num = nums[i];
                count = 1;
            }
        }

        return cand_num;
    }
}
