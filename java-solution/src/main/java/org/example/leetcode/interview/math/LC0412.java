package org.example.leetcode.interview.math;

import java.util.LinkedList;
import java.util.List;

public class LC0412 {
    public List<String> fizzBuzz(int n) {
        List<String> ans = new LinkedList<>();

        for (int i = 1; i <= n; i++) {
            if ((i % 3 == 0) && (i % 5 == 0)) {
                ans.add("FizzBuzz");
            } else if (i % 3 == 0) {
                ans.add("Fizz");
            } else if (i % 5 == 0) {
                ans.add("Buzz");
            } else {
                ans.add(String.valueOf(i));
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        LC0412 solu = new LC0412();
        System.out.println(solu.fizzBuzz(1));
        System.out.println(solu.fizzBuzz(3));
        System.out.println(solu.fizzBuzz(5));
        System.out.println(solu.fizzBuzz(15));
    }
}
