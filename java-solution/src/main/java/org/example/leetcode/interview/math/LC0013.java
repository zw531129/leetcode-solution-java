package org.example.leetcode.interview.math;

import java.util.HashMap;

public class LC0013 {
    private static final HashMap<Character, Integer> ROMAN = new HashMap<Character, Integer>() {{
        put('I', 1);
        put('V', 5);
        put('X', 10);
        put('L', 50);
        put('C', 100);
        put('D', 500);
        put('M', 1000);
    }};

    public int romanToInt(String s) {
        int ans = 0;
        int ptr = 0;
        while (ptr < s.length()) {
            char ch = s.charAt(ptr);
            int val = ROMAN.get(ch);
            if (ptr + 1 < s.length() && val < ROMAN.get(s.charAt(ptr + 1))) {
                ans += ROMAN.get(s.charAt(ptr + 1)) - val;
                ptr += 2;
            } else {
                ans += ROMAN.get(ch);
                ptr++;
            }
        }

        return ans;
    }

    public static void main(String[] args) {
        LC0013 solu = new LC0013();
        solu.romanToInt("III");
        solu.romanToInt("IV");
        solu.romanToInt("IX");
        solu.romanToInt("LVIII");
        solu.romanToInt("MCMXCIV");
    }
}
