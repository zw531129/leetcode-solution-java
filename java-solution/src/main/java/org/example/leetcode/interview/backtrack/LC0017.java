package org.example.leetcode.interview.backtrack;

import java.util.ArrayList;
import java.util.List;

public class LC0017 {
    List<String> ans = new ArrayList<>();
    StringBuilder builder = new StringBuilder();
    String[] keyboard = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    public List<String> letterCombinations(String digits) {
        if (digits == null || digits.length() == 0) {
            return ans;
        }
        // num从0开始
        backTracking(digits, 0);

        return ans;
    }

    private void backTracking(String digits, int num) {
        if (num == digits.length()) {
            ans.add(builder.toString());
            return;
        }

        String str = keyboard[digits.charAt(num) - '0'];
        for (int i = 0; i < str.length(); i++) {
            builder.append(str.charAt(i));
            backTracking(digits, num + 1);
            builder.deleteCharAt(builder.length() - 1);
        }
    }
}
