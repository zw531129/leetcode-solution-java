package org.example.leetcode.interview.slide;

import java.util.HashSet;
import java.util.Set;

public class LC0003 {
    /**
     * 滑动窗口找出没有重复的字符串, 用set来检测是否有重复字符串
     * 不能在while里面统计字符串的长度, 不确定是否有没删干净的重复元素或者还有新的未重复元素加入
     * @param s
     * @return
     */
    public int lengthOfLongestSubstring(String s) {
        int ans = Integer.MIN_VALUE;
        Set<Character> set = new HashSet<>();
        int left = 0;
        for (int right = 0; right < s.length(); right++) {
            char ch = s.charAt(right);
            while (set.contains(ch)) {
                set.remove(s.charAt(left++));
            }
            set.add(ch);
            ans = Math.max(ans, right - left + 1);
        }
        // 处理返回值, 没有符合要求的返回是0
        return ans == Integer.MIN_VALUE ? 0 : ans;
    }

    public static void main(String[] args) {
        LC0003 solu = new LC0003();
        solu.lengthOfLongestSubstring("abcabcbb");
        solu.lengthOfLongestSubstring("bbbbb");
        solu.lengthOfLongestSubstring("pwwkew");
    }
}
