package org.example.leetcode.interview.string;

public class LC0242 {
    public boolean isAnagram(String s, String t) {
        int[] record = new int[26];

        for (char ch : s.toCharArray()) {
            record[ch - 'a'] += 1;
        }
        for (char ch : t.toCharArray()) {
            record[ch - 'a'] -= 1;
        }

        for (int each : record) {
            if (each != 0) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        LC0242 solu = new LC0242();
        System.out.println(solu.isAnagram("anagram", "nagaram"));
        System.out.println(solu.isAnagram("rat", "car"));
    }
}
