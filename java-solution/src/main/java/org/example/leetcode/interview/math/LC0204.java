package org.example.leetcode.interview.math;

public class LC0204 {
    public int countPrimes(int n) {
        int count = 0;
        boolean[] signs = new boolean[n];
        for (int i = 2; i < n; i++) {
            //因为在 C# 中，布尔类型的默认值为 假。所以在此处用了逻辑非（！）操作符。
            if (!signs[i]) {
                count++;
                for (int j = i + i; j < n; j += i) {
                    //排除不是质数的数
                    signs[j] = true;
                }
            }
        }
        return count;
    }
}
