package org.example.leetcode.interview.math;

public class LC0172 {
    public int trailingZeroes(int n) {
        // 统计5的个数
        int count = 0;
        while (n >= 5) {
            count += n / 5;
            n /= 5;
        }

        return count;
    }
}
