package org.example.leetcode.addon.lc59;

public class Solution {
    public int[][] generateMatrix(int n) {
        // 确认边界
        int left = 0;
        int up = 0;
        int right = n - 1;
        int down = n - 1;
        // 头和尾
        int head = 1;
        int tail = n * n;

        int [][]ans = new int[n][n];
        // 循环到目标值
        while (head <= tail) {
            for (int i = left; i <= right; i++) {
                ans[up][i] = head++;
            }
            ++up;

            for (int i = up; i <= down; i++) {
                ans[i][right] = head++;
            }
            --right;

            for (int i = right; i >= left; i--) {
                ans[down][i] = head++;
            }
            --down;

            for (int i = down; i >= up; i--) {
                ans[i][left] = head++;
            }
            ++left;
        }

        return ans;
    }

    public static void main(String[] args) {

    }
}
