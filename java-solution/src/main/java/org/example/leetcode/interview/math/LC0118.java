package org.example.leetcode.interview.math;

import java.util.LinkedList;
import java.util.List;

public class LC0118 {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ans = new LinkedList<>();
        for (int i = 0; i < numRows; i++) {
            List<Integer> row = new LinkedList<>();
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i) {
                    row.add(1);
                } else {
                    row.add(ans.get(i - 1).get(j - 1) + ans.get(i - 1).get(j));
                }
            }
            ans.add(row);
        }

        return ans;
    }

    public static void main(String[] args) {
        LC0118 solu = new LC0118();
        solu.generate(5);
    }
}
