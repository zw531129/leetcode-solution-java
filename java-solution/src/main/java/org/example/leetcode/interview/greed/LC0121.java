package org.example.leetcode.interview.greed;

public class LC0121 {
    public int maxProfit(int[] prices) {
        int low = Integer.MAX_VALUE;
        int ans = 0;
        for (int i = 0;  i < prices.length; i++) {
            low = Math.min(prices[i], low);
            ans = Math.max(prices[i] - low, ans);
        }

        return ans;
    }

    public static void main(String[] args) {
        LC0121 solu = new LC0121();
        solu.maxProfit(new int[] {7,1,5,3,6,4});
        solu.maxProfit(new int[] {7,6,4,3,1});
    }
}
