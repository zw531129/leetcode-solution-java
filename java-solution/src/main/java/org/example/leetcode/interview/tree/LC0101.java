package org.example.leetcode.interview.tree;

import org.example.util.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

public class LC0101 {
    public boolean isSymmetric(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root.left);
        queue.offer(root.right);

        while (!queue.isEmpty()) {
            TreeNode leftNode = queue.poll();
            TreeNode rightNode = queue.poll();

            if (leftNode == rightNode) {
                continue;
            }

            if ((leftNode == null || rightNode == null || leftNode.val != rightNode.val)) {
                return false;
            }

            queue.offer(leftNode.left);
            queue.offer(rightNode.right);
            queue.offer(rightNode.left);
            queue.offer(leftNode.right);
        }

        return true;
    }
}
