package org.example.leetcode.interview.string;

public class LC0344 {
    public void reverseString(char[] s) {
        int left = 0;
        int right = s.length - 1;

        while (left <= right) {
            char tmp = s[left];
            s[left] = s[right];
            s[right] = tmp;
            ++left;
            --right;
        }
    }

    public static void main(String[] args) {
        LC0344 solu = new LC0344();
        solu.reverseString(new char[]{'h', 'e', 'l', 'l', 'o'});
        solu.reverseString(new char[]{'H', 'a', 'n', 'n', 'a', 'h'});
    }
}
