package org.example.leetcode.interview.doublepointer;

import org.example.util.IO;

public class LC0283 {
    public void moveZeroes(int[] nums) {
        int left = 0;
        int right = 0;

        while (right < nums.length) {
            if (nums[right] == 0) {
                ++right;
            } else {
                int tmp = nums[left];
                nums[left] = nums[right];
                nums[right] = tmp;
                ++left;
                ++right;
            }
        }
    }

    public static void main(String[] args) {
        LC0283 solu = new LC0283();
        solu.moveZeroes(IO.intArray("[0,1,0,3,12]"));
        solu.moveZeroes(IO.intArray("[0]"));
    }
}
