package org.example.leetcode.addon.lc58;

public class Solution {
    public int lengthOfLastWord(String s) {
        int counter = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == ' ') {
                if (counter == 0) continue;
                break;
            }
            counter++;
        }

        return counter;
    }

    public static void main(String[] args) {
        Solution solu = new Solution();
        solu.lengthOfLastWord("a");
//        solu.lengthOfLastWord("Hello World");
//        solu.lengthOfLastWord("   fly me   to   the moon  ");
//        solu.lengthOfLastWord("luffy is still joyboy");
    }
}

