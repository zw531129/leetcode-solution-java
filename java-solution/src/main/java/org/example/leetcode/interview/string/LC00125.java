package org.example.leetcode.interview.string;

public class LC00125 {
    public boolean isPalindrome(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetterOrDigit(s.charAt(i))) {
                builder.append(Character.toLowerCase(s.charAt(i)));
            }
        }

        int left = 0;
        int right = builder.toString().length() - 1;
        char[] cArray = builder.toString().toCharArray();
        while (left < right) {
            char lAlpha = cArray[left];
            char rAlpha = cArray[right];
            if (lAlpha != rAlpha) {
                return false;
            }
            left++;
            right--;
        }

        return true;
    }

    public static void main(String[] args) {
        LC00125 solu = new LC00125();
        solu.isPalindrome("A man, a plan, a canal: Panama");
        solu.isPalindrome("race a car");
        solu.isPalindrome(" ");
    }
}
