package org.example.leetcode.interview.doublepointer;

public class LC0287 {
    public int findDuplicate(int[] nums) {
        int slow = 0;
        int fast = 0;

        while(true) {
            fast = nums[nums[fast]];
            slow = nums[slow];
            if (fast == slow) {
                break;
            }
        }

        int pos = 0;
        while(true) {
            pos = nums[pos];
            slow = nums[slow];
            if (slow == pos) {
                break;
            }
        }

        return slow;
    }
}
