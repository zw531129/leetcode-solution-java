package org.example.leetcode.interview.matrix;

public class LC0073 {
    public void setZeroes(int[][] matrix) {
        int row = matrix.length;
        int col = matrix[0].length;
        boolean row0Flag = false;
        boolean col0Flag = false;
        // 第一行是否有0
        for (int j = 0; j < col; j++) {
            if (matrix[0][j] == 0) {
                row0Flag = true;
                break;
            }
        }
        // 第一列是否有0
        for (int i = 0; i < row; i++) {
            if (matrix[i][0] == 0) {
                col0Flag = true;
                break;
            }
        }
        // mark, 点等于0该行和该列都是0
        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                if (matrix[i][j] == 0) {
                    matrix[i][0] = matrix[0][j] = 0;
                }
            }
        }
        // 某行或某列有0, 那么这两条线的交点也是0
        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                if (matrix[i][0] == 0 || matrix[0][j] == 0) {
                    matrix[i][j] = 0;
                }
            }
        }
        if (row0Flag) {
            for (int j = 0; j < col; j++) {
                matrix[0][j] = 0;
            }
        }
        if (col0Flag) {
            for (int i = 0; i < row; i++) {
                matrix[i][0] = 0;
            }
        }
    }
}
