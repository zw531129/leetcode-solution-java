package org.example.leetcode.interview.slide;

public class LC0028 {
    /**
     * 双循环, 外循环是needle的长度(一块), 每次前进一块
     * 内循环是遍历needle, 看和haystack每一位字符是否对的上
     * 每结束一层外循环后判断一下是否包含, 包含的话返回对应的下标i
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr(String haystack, String needle) {
        int hLength = haystack.length();
        int nLength = needle.length();

        for (int i = 0; i + nLength <= hLength; i++) {
            boolean isMatch = true;
            for (int j = 0; j < nLength; j++) {
                if (haystack.charAt(i + j) != needle.charAt(j)) {
                    isMatch = false;
                }
            }
            if (isMatch) {
                return i;
            }
        }

        return -1;
    }

    public static void main(String[] args) {

    }
}
