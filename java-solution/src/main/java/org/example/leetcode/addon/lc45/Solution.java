package org.example.leetcode.addon.lc45;

public class Solution {
    public int jump(int[] nums) {
        if (nums == null || nums.length == 0 || nums.length == 1) {
            return 0;
        }

        int count = 0;
        int maxDist = 0;
        int currDist = 0;

        for (int i = 0; i <= maxDist; i++) {
            maxDist = Math.max(maxDist, i + nums[i]);
            if (maxDist >= nums.length - 1) {
                count++;
                break;
            }
            if (i == currDist) {
                currDist = maxDist;
                count++;
            }
        }

        return count;
    }
}
