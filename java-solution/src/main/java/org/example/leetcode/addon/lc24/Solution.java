package org.example.leetcode.addon.lc24;

import org.example.util.IO;
import org.example.util.ListNode;

public class Solution {
    public ListNode swapPairs(ListNode head) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode curr = dummy;

        while (curr.next != null && curr.next.next != null) {
            ListNode cloest = curr.next;
            ListNode furthest = curr.next.next.next;

            curr.next = curr.next.next;
            curr.next.next = cloest;
            curr.next.next.next = furthest;

            curr = curr.next.next;
        }

        return dummy.next;
    }

    public static void main(String[] args) {
        Solution solu = new Solution();
        System.out.println(IO.showLinedList(solu.swapPairs(IO.intLinkedList("[1,2,3,4]"))));
        System.out.println(IO.showLinedList(solu.swapPairs(IO.intLinkedList("[]"))));
        System.out.println(IO.showLinedList(solu.swapPairs(IO.intLinkedList("[1]"))));
    }
}
