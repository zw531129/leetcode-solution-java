package org.example.leetcode.interview.math;

import java.util.Arrays;

public class LC0179 {
    public String largestNumber(int[] nums) {
        int n = nums.length;
        String[] strNums = new String[n];
        for (int i = 0; i < n; i++) {
            strNums[i] = String.valueOf(nums[i]);
        }

        Arrays.sort(strNums, (a, b) -> (b + a).compareTo((a + b)));
        //如果排序后的第一个元素是0，那后面的元素肯定小于或等于0，则可直接返回0
        if(strNums[0].equals("0")){
            return "0";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(strNums[i]);
        }

        return sb.toString();
    }
}
