package org.example.leetcode.interview.hash;

import java.util.HashSet;
import java.util.Set;

public class LC0217 {
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> bucket = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (bucket.contains(nums[i])) {
                return true;
            }
            bucket.add(nums[i]);
        }

        return false;
    }
}
