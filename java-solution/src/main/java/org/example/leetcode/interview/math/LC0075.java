package org.example.leetcode.interview.math;

public class LC0075 {
    public void sortColors(int[] nums) {
        int left = 0;
        int right = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                swap(nums, i, left);
                if (left < right) {
                    swap(nums, i, right);
                }
                ++left;
                ++right;
            } else if (nums[i] == 1) {
                swap(nums, i, right);
                ++right;
            }
        }
    }

    private void swap(int[] nums, int p1, int p2) {
        int temp = nums[p1];
        nums[p1] = nums[p2];
        nums[p2] = temp;
    }
}
