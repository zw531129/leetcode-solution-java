package org.example.leetcode.addon.lc199;

import org.example.util.TreeNode;

import java.util.*;

public class Solution {
    public List<Integer> rightSideView(TreeNode root) {
        if (root == null) return Collections.EMPTY_LIST;

        List<Integer> ans = new LinkedList<>();
        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);

        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                if (node.left != null) queue.offer(node.left);
                if (node.right != null) queue.offer(node.right);

                if (i == size - 1) ans.add(node.val);

            }
        }

        return ans;
    }

    public static void main(String[] args) {

    }
}
