package org.example.leetcode.interview.math;

public class LC0136 {
    public int singleNumber(int[] nums) {
        int single = 0;
        for (int num : nums) {
            single ^= num;
        }

        return single;
    }
}
