package org.example.leetcode.interview.doublepointer;

import org.example.util.IO;

public class LC0026 {
    public int removeDuplicates(int[] nums) {
        int left = 0;
        int right = 0;

        while (right < nums.length) {
            if (nums[left] == nums[right]) {
                ++right;
            } else {
                nums[++left] = nums[right++];
            }
        }

        return left + 1;
    }

    public static void main(String[] args) {
        LC0026 solu = new LC0026();
        System.out.println(solu.removeDuplicates(IO.intArray("[1,1,2]")));
        System.out.println(solu.removeDuplicates(IO.intArray("[0,0,1,1,1,2,2,3,3,4]")));
    }
}
