package org.example.leetcode.interview.linkedlist;

import java.util.HashMap;
import java.util.Map;

public class LC0138 {
    public Node copyRandomList(Node head) {
        if (head == null) return head;

        Node curr = head;
        Map<Node, Node> map = new HashMap<>();
        while (curr != null) {
            Node clone = new Node(curr.val);
            map.put(curr, clone);
            curr = curr.next;
        }

        // reset
        curr = head;
        while (curr != null) {
            Node clone = map.get(curr);
            clone.next = map.get(curr.next);
            clone.random = map.get(curr.random);
            curr = curr.next;
        }

        return map.get(head);
    }
}

class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}