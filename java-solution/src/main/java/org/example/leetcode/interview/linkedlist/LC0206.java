package org.example.leetcode.interview.linkedlist;

import org.example.util.IO;
import org.example.util.ListNode;

public class LC0206 {
    public ListNode reverseList(ListNode head) {
        ListNode dummy = null;
        ListNode prev = dummy;
        ListNode curr = head;

        while (curr != null) {
            ListNode tmp = curr.next;
            curr.next = prev;
            prev = curr;
            curr = tmp;
        }

        return prev;
    }

    public static void main(String[] args) {
        LC0206 solu = new LC0206();
        System.out.println(IO.showLinedList(solu.reverseList(IO.intLinkedList("[1,2,3,4,5]"))));
        System.out.println(IO.showLinedList(solu.reverseList(IO.intLinkedList("[1,2]"))));
        System.out.println(IO.showLinedList(solu.reverseList(IO.intLinkedList("[]"))));
    }
}
