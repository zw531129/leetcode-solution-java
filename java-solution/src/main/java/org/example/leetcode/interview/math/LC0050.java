package org.example.leetcode.interview.math;

public class LC0050 {
    /**
     * 0.0的任何幂都是0
     * 如果n是负数, 就求几个1/x相乘
     * @param x
     * @param n
     * @return
     */
    public double myPow(double x, int n) {
        if (x == 0.0f) return 0.0d;
        long exp = n;
        double ans = 1.0;
        if (exp < 0) {
            x = 1 / x;
            exp = -exp;
        }
        while (exp > 0) {
            if ((exp & 1) == 1) ans *= x;
            x *= x;
            exp >>= 1;
        }

        return ans;
    }

    public static void main(String[] args) {
        LC0050 solu = new LC0050();
        solu.myPow(2.0, 5);
    }
}
