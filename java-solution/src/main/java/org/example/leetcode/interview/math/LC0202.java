package org.example.leetcode.interview.math;

import java.util.HashSet;
import java.util.Set;

public class LC0202 {
    public boolean isHappy(int n) {
        Set<Integer> record = new HashSet<>();
        while (n != 1 && !record.contains(n)) {
            record.add(n);
            n = getNextNumber(n);
        }

        return n == 1;
    }

    private int getNextNumber(int n) {
        int res = 0;
        while (n > 0) {
            int temp = n % 10;
            res += temp * temp;
            n /= 10;
        }

        return res;
    }

    public static void main(String[] args) {
        LC0202 solu = new LC0202();
        solu.isHappy(19);
        solu.isHappy(2);
    }
}
