package org.example.leetcode.interview.greed;

public class LC0055 {
    public boolean canJump(int[] nums) {
        if (nums.length == 1) {
            return true;
        }

        int cover = 0;
        for (int i = 0; i <= cover; i++) {
            cover = Math.max(cover, i + nums[i]);
            if (cover >= nums.length - 1) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        LC0055 solu = new LC0055();

        solu.canJump(new int[]{2, 3, 1, 1, 4});
    }
}
