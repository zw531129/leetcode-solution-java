package org.example.leetcode.interview.backtrack;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class LC0046 {
    List<List<Integer>> ans = new LinkedList<>();
    Deque<Integer> path = new LinkedList<>();
    boolean[] used;

    public List<List<Integer>> permute(int[] nums) {
        if (nums.length == 0) return ans;

        used = new boolean[nums.length];
        backtracking(nums);

        return ans;
    }

    private void backtracking(int[] nums) {
        if (path.size() == nums.length) {
            ans.add(new LinkedList<>(path));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            if (used[i]) {
                continue;
            }

            used[i] = true;
            path.add(nums[i]);
            backtracking(nums);
            path.removeLast();
            used[i] = false;
        }
    }
}
