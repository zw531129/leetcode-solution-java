package org.example.leetcode.interview.math;

public class LC0171 {
    public int titleToNumber(String columnTitle) {
        int ans = 0;
        for (int i = 0; i < columnTitle.length(); i++) {
            char ch = columnTitle.charAt(i);
            int num = ch - 'A' + 1;
            ans = ans * 26 + num;
        }

        return ans;
    }
}
