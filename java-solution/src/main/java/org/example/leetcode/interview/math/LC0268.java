package org.example.leetcode.interview.math;

public class LC0268 {
    public int missingNumber(int[] nums) {
        int n = nums.length;
        int curr = 0;
        int sum = n * (n + 1) / 2;
        for (int i : nums) {
            curr += i;
        }

        return sum - curr;
    }
}
