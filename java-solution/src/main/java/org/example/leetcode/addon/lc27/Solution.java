package org.example.leetcode.addon.lc27;

import org.example.util.IO;

public class Solution {
    public int removeElement(int[] nums, int val) {
        int left = 0;
        int right = 0;

        while (right < nums.length) {
            if (nums[right] == val) {
                ++right;
            } else {
                nums[left] = nums[right];
                ++left;
                ++right;
            }
        }

        return left;
    }

    public static void main(String[] args) {
        Solution solu = new Solution();
        System.out.println(solu.removeElement(IO.intArray("[3,2,2,3]"), 3));
        System.out.println(solu.removeElement(IO.intArray("[0,1,2,2,3,0,4,2]"), 2));
    }
}
