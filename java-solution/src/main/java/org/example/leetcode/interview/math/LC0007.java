package org.example.leetcode.interview.math;

public class LC0007 {
    /**
     * 从低到高求每一位的值, 最后加起来就是翻转的整数
     * 每次需要判断加起来的数是否超过边界，超过边界则返回0
     *
     * @param x
     * @return
     */
    public int reverse(int x) {
        int ans = 0;
        while (x != 0) {
            // 判断边界, 后面要除10, 小于或大于边界值除10都算越界
            if (ans < Integer.MIN_VALUE / 10 || ans > Integer.MAX_VALUE / 10) {
                return 0;
            }

            int digit = x % 10;
            x /= 10;
            ans = ans * 10 + digit;
        }

        return ans;
    }

    public static void main(String[] args) {
        LC0007 solu = new LC0007();
        System.out.println(solu.reverse(123));
        System.out.println(solu.reverse(-123));
        System.out.println(solu.reverse(120));
        System.out.println(solu.reverse(0));
        System.out.println(solu.reverse(901000));
    }
}
