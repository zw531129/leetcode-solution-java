package org.example.leetcode.interview.doublepointer;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LC0015 {
    /**
     * 数组排序从左往右遍历, 配合双指针左右相向运动
     * @param nums
     * @return
     */
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> ans =new LinkedList<>();
        Arrays.sort(nums);

        for (int i = 0; i < nums.length; i++) {
            // 不存在最左边大于0，abc之和还能等于0的组合
            if (nums[i] > 0) {
                return ans;
            }
            // 题目不允许重复nums[i] == nums[i - 1]就跳过
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }

            int left = i + 1;
            int right = nums.length - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                if (sum > 0) {
                    right--;
                } else if (sum < 0) {
                    left++;
                } else {
                    ans.add(Arrays.asList(nums[i], nums[left], nums[right]));
                    // 去重, 右指针左移下一位重复, 移动到不重复为止
                    while (right > left && nums[right] == nums[right - 1]) right--;
                    // 去重, 左指针右移下一位重复, 移动到不重复为止
                    while (right > left && nums[left] == nums[left + 1]) left++;
                    // 正常移动指针
                    right--;
                    left++;
                }
            }
        }

        return ans;
    }
}
