package org.example.leetcode.interview.slide;

import java.util.HashSet;
import java.util.Set;

public class LC0128 {
    public int longestConsecutive(int[] nums) {
        if (nums.length <= 0) {
            return 0;
        }
        // 去重
        Set<Integer> hashSet = new HashSet<>();
        for (int tmp : nums) {
            hashSet.add(tmp);
        }

        int res = 0;
        for (Integer x : hashSet) {
            if (!hashSet.contains(x - 1)) {
                // x是第一个没有前驱的元素, 是起始元素
                int y = x;
                while (hashSet.contains(y + 1)) {
                    y++;
                }
                // 最大值 - 起始值 + 1算个数
                res = Math.max(res, y - x + 1);
            }
        }

        return res;
    }
}
