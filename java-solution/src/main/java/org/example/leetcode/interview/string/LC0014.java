package org.example.leetcode.interview.string;

public class LC0014 {
    /**
     * 以strs[0]作为基准, 循环时候比较每一列第i个元素是否等于第一行的第i个元素
     *
     * @param strs
     * @return
     */
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        int col = strs[0].length();
        int row = strs.length;

        for (int i = 0; i < col; i++) {
            char ch = strs[0].charAt(i);
            for (int j = 1; j < row; j++) {
                // i == strs[j].length()证明爆了, 不用比较了
                if (i == strs[j].length() || strs[j].charAt(i) != ch) {
                    return strs[0].substring(0, i);
                }
            }
        }

        return strs[0];
    }

    public static void main(String[] args) {
        LC0014 solu = new LC0014();
        solu.longestCommonPrefix(new String[]{"flower","flow","flight"});
        solu.longestCommonPrefix(new String[]{"dog","racecar","car"});
    }
}
