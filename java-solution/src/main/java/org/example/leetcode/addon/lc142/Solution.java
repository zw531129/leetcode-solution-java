package org.example.leetcode.addon.lc142;

import org.example.util.ListNode;

public class Solution {
    public ListNode detectCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;

            if (fast == slow) {
                ListNode idx1 = fast;
                ListNode idx2 = head;

                while (idx1 != idx2) {
                    idx1 = idx1.next;
                    idx2 = idx2.next;
                }

                return idx1;
            }
        }

        return null;
    }

    public static void main(String[] args) {
        Solution solu = new Solution();
    }
}
