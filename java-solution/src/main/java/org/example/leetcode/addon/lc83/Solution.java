package org.example.leetcode.addon.lc83;

import org.example.util.IO;
import org.example.util.ListNode;

public class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode curr = dummy;

        while (curr != null && curr.next != null) {
            if (curr.val == curr.next.val) {
                curr.next = curr.next.next;
            } else {
                curr = curr.next;
            }
        }
        return dummy.next;
    }

    public static void main(String[] args) {
        Solution solu = new Solution();
        solu.deleteDuplicates(IO.intLinkedList("[1,1,2]"));
    }
}
