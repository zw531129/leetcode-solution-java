package org.example.leetcode.addon.lc35;

import org.example.util.IO;

public class Solution {
    public int searchInsert(int[] nums, int target) {
        int left = 0;
        int right = nums.length;

        while (left < right) {
            int mid = left + ((right - left) / 2);
            if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        return left;
    }

    public static void main(String[] args) {
        Solution solu = new Solution();
        System.out.println(
                solu.searchInsert(IO.intArray("[1,3,5,6]"), 5)
        );
        System.out.println(
                solu.searchInsert(IO.intArray("[1,3,5,6]"), 2)
        );
        System.out.println(
                solu.searchInsert(IO.intArray("[1,3,5,6]"), 7)
        );

    }
}
