package org.example.leetcode.interview.stack;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;

public class LC0150 {
    public int evalRPN(String[] tokens) {
        List<String> operator = Arrays.asList("+", "-", "*", "/");
        Deque<String> stack = new ArrayDeque<>();

        for (String str : tokens) {
            if (operator.contains(str)) {
                int right = Integer.parseInt(stack.pop());
                int left = Integer.parseInt(stack.pop());
                long val = Integer.MIN_VALUE;
                switch (str) {
                    case "+":
                        val = left + right;
                        break;
                    case "-":
                        val = left - right;
                        break;
                    case "*":
                        val = left * right;
                        break;
                    case "/":
                        val = left / right;
                        break;
                }
                stack.push(String.valueOf(val));
            } else {
                stack.push(str);
            }
        }

        return Integer.parseInt(stack.pop());
    }

    public static void main(String[] args) {
        LC0150 solu = new LC0150();
        solu.evalRPN(new String[]{"2", "1", "+", "3", "*"});
        solu.evalRPN(new String[]{"4", "13", "5", "/", "+"});
        solu.evalRPN(new String[]{"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"});
    }
}
