package org.example.leetcode.interview.tree;

import org.example.util.TreeNode;

import java.util.Deque;
import java.util.LinkedList;

public class LC0098 {
    public boolean isValidBST(TreeNode root) {
        if (root == null) return false;

        Deque<TreeNode> stack = new LinkedList<>();
        TreeNode curr = root;
        TreeNode pre = null;

        while (curr != null || !stack.isEmpty()) {
            if (curr != null) {
                stack.push(curr);
                curr = curr.left;
            } else {
                curr = stack.pop();
                if (pre != null && curr.val <= pre.val) {
                    return false;
                }
                // 记录上一个节点
                pre = curr;
                curr = curr.right;
            }
        }

        return true;
    }
}
