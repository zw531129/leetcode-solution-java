package org.example.leetcode.interview.doublepointer;

public class LC0088 {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] ans = new int[m + n];
        int curr = ans.length - 1;
        int mPtr = m - 1;
        int nPtr = n - 1;

        while (mPtr >= 0 || nPtr >= 0) {
            if (mPtr == -1) {
                ans[curr--] = nums2[nPtr--];
            } else if (nPtr == -1) {
                ans[curr--] =  nums1[mPtr--];
            } else if (nums1[mPtr] > nums2[nPtr]) {
                ans[curr--] = nums1[mPtr--];
            } else {
                ans[curr--] = nums2[nPtr--];
            }
        }

        System.arraycopy(ans, 0, nums1, 0, nums1.length);
    }

    public static void main(String[] args) {
        LC0088 solu = new LC0088();
        solu.merge(new int[]{1, 2, 3, 0, 0, 0}, 3, new int[]{2, 5, 6}, 3);
        solu.merge(new int[]{1}, 1, new int[]{}, 0);
        solu.merge(new int[]{0}, 1, new int[]{1}, 0);
        solu.merge(new int[]{2, 0}, 1, new int[]{1}, 1);
    }
}
