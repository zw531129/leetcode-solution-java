package org.example.leetcode.addon.lc9;

public class Solution {
    public boolean isPalindrome(int x) {
        String ipt = String.valueOf(x);
        int left = 0;
        int right = ipt.length() - 1;
        while (left <= right) {
            if (ipt.charAt(left) != ipt.charAt(right)) {
                return false;
            }
            ++left;
            --right;
        }

        return true;

    }

    public static void main(String[] args) {
        Solution solu = new Solution();
        System.out.println(solu.isPalindrome(121));
        System.out.println(solu.isPalindrome(-121));
        System.out.println(solu.isPalindrome(10));
    }
}
