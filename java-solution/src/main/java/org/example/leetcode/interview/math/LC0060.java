package org.example.leetcode.interview.math;

public class LC0060 {
    /**
     * 判断是否是9, 不是的话直接+1
     * 是9的话当前位置0, 最后数组多开一位置1(等同于进位)
     *
     * @param digits
     * @return
     */
    public int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            if (digits[i] != 9) {
                digits[i]++;
                return digits;
            }
            digits[i] = 0;
        }

        int[] ans = new int[digits.length + 1];
        ans[0] = 1;

        return ans;
    }
}
