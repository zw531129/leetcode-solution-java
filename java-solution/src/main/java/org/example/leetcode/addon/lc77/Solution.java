package org.example.leetcode.addon.lc77;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Solution {
    List<List<Integer>> ans = new ArrayList<>();
    Deque<Integer> path = new LinkedList<>();

    public List<List<Integer>> combine(int n, int k) {
        int[] ints = new int[n];
        for (int i = 0; i < n; i++) {
            ints[i] = i + 1;
        }
        backTracking(ints, k, 1);

        return ans;
    }

    private void backTracking(int[] ints, int k, int startIndex) {
        if (path.size() == k) {
            ans.add(new LinkedList<>(path));
            return;
        }

        for (int i = startIndex; i <= ints.length - (k  - path.size()) + 1; i++) {
            path.add(i);
            backTracking(ints, k, i + 1);
            path.removeLast();
        }
    }
}
