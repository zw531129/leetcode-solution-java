package org.example.leetcode.interview.doublepointer;

public class LC0011 {
    public int maxArea(int[] height) {
        int size = height.length;
        int left = 0;
        int right = size - 1;
        int ans = 0;

        while (left < right) {
            // 容器底部移动1格子
            // 移动高较小的指针，企图获得更大的高，这样面积最大
            ans = Math.max(ans, (right - left) * Math.min(height[left], height[right]));
            if (height[left] > height[right]) {
                right--;
            } else {
                left++;
            }
        }

        return ans;
    }
}
