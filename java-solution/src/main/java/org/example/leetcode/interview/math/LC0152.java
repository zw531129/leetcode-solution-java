package org.example.leetcode.interview.math;

public class LC0152 {
    public int maxProduct(int[] nums) {
        int ans = Integer.MIN_VALUE;
        int iMax = 1;
        int iMin = 1;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < 0) {
                // swap min max
                int tmp = iMax;
                iMax = iMin;
                iMin = tmp;
            }

            iMax = Math.max(iMax * nums[i], nums[i]);
            iMin = Math.min(iMin * nums[i], nums[i]);
            ans = Math.max(ans, iMax);
        }

        return ans;
    }
}
