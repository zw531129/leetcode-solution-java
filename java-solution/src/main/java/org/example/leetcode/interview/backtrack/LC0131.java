package org.example.leetcode.interview.backtrack;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class LC0131 {
    List<List<String>> ans = new ArrayList<>();
    Deque<String> strings = new LinkedList<>();

    public List<List<String>> partition(String s) {
        backTracking(s, 0);
        return ans;
    }

    private void backTracking(String s, int startIndex) {
        if (startIndex >= s.length()) {
            ans.add(new ArrayList<>(strings));

            return;
        }

        for (int i = startIndex; i < s.length(); i++) {
            if (!isPalindrome(s, startIndex, i)) {
                continue;
            } else {
                String str = s.substring(startIndex, i + 1);
                strings.addLast(str);
            }

            backTracking(s, i + 1);
            strings.removeLast();
        }
    }

    private boolean isPalindrome(String s, int startIndex, int end) {
        for (int i = startIndex, j = end; i < j; i++, j--) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
        }
        return true;
    }
}
