package org.example.leetcode.interview.string;

public class LC0008 {
    /**
     * 指针index要一直跟着, 根据要求从开始到结尾
     * @param s
     * @return
     */
    public int myAtoi(String s) {
        int len = s.length();
        // str.charAt(i) 方法回去检查下标的合法性，一般先转换成字符数组
        char[] charArray = s.toCharArray();

        // 1、去除前导空格
        int index = 0;
        while (index < len && charArray[index] == ' ') {
            index++;
        }

        // 2、如果已经遍历完成（针对极端用例 "      "）
        if (index == len) {
            return 0;
        }

        // 3、如果出现符号字符，仅第 1 个有效，并记录正负
        int sign = 1;
        char symbol = charArray[index];
        if (symbol == '+') {
            index++;
        } else if (symbol == '-') {
            index++;
            sign = -1;
        }

        // 4、将后续出现的数字字符进行转换
        // 不能使用 long 类型，这是题目说的
        int ans = 0;
        while (index < len) {
            char currChar = charArray[index];
            // 4.1 先判断不合法的情况
            if (currChar > '9' || currChar < '0') {
                break;
            }

            int digit = currChar - '0';

            // 题目中说：环境只能存储 32 位大小的有符号整数，因此，需要提前判：断乘以 10 以后是否越界
            // 比较最后一位 ans == Integer.MAX_VALUE / 10 && digit > Integer.MAX_VALUE % 10
            if (ans > Integer.MAX_VALUE / 10 || (ans == Integer.MAX_VALUE / 10 && digit > Integer.MAX_VALUE % 10)) {
                return Integer.MAX_VALUE;
            }
            if (ans < Integer.MIN_VALUE / 10 || (ans == Integer.MIN_VALUE / 10 && digit > -(Integer.MIN_VALUE % 10))) {
                return Integer.MIN_VALUE;
            }

            // 4.2 合法的情况下，才考虑转换，每一步都把符号位乘进去
            ans = ans * 10 + sign * digit;
            index++;
        }

        return ans;
    }

    public static void main(String[] args) {
        LC0008 solu = new LC0008();
        System.out.println(solu.myAtoi("42"));
        System.out.println(solu.myAtoi("-42"));
        System.out.println(solu.myAtoi("    4193 with words  "));
        System.out.println(solu.myAtoi("words and 987"));
        System.out.println(solu.myAtoi("3.14"));
        System.out.println(solu.myAtoi(".14"));
        System.out.println(solu.myAtoi("-123456789000"));
        System.out.println(solu.myAtoi("3147483647"));
    }

}
