package org.example.leetcode.interview.string;

import java.util.*;

public class LC0049 {
    /**
     * 排序以后相等就是异位词
     *
     * @param strs
     * @return
     */
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();
        for (String each : strs) {
            char[] chars = each.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);
            List<String> value = map.getOrDefault(key, new LinkedList<>());
            value.add(each);
            map.put(key, value);
        }

        return new LinkedList<>(map.values());
    }
}
