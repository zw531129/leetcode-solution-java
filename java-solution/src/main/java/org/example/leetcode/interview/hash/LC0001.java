package org.example.leetcode.interview.hash;

import java.util.HashMap;
import java.util.Map;

public class LC0001 {
    /**
     * 题目规定了每个答案有唯一解, 使用哈希表解题最快
     * 遍历整个数组, 计算出另一个配对的数(target - num[i]), 如果哈希表中有就捞出来对应的位置(value), 没有就放进去对应的值和位置作为k-v.
     * 不能一开始就遍历加进去，这样会重复选到某个元素.
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int first = nums[i];
            int need = target - first;
            if (map.containsKey(need)) {
                int pos = map.get(need);
                return new int[]{i, pos};
            } else {
                map.put(nums[i], i);
            }
        }

        return new int[]{-1, -1};
    }

    public static void main(String[] args) {
        LC0001 solu = new LC0001();
        solu.twoSum(new int[]{2, 7, 11, 15}, 9);
        solu.twoSum(new int[]{3, 2, 4}, 6);
        solu.twoSum(new int[]{3, 3}, 6);
    }
}
