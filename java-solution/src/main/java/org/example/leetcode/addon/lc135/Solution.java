package org.example.leetcode.addon.lc135;

public class Solution {
    public int candy(int[] ratings) {
        int[] candis = new int[ratings.length];
        candis[0] = 1;

        for (int i = 1; i < ratings.length; i++) {
            candis[i] = (ratings[i] > ratings[i - 1]) ? candis[i - 1] + 1 : 1;
        }

        for (int i = ratings.length - 2; i >= 0; i--) {
            if (ratings[i] > ratings[i + 1]) {
                candis[i] = Math.max(candis[i], candis[i + 1] + 1);
            }
        }

        int ans = 0;
        for (int num : candis) {
            ans += num;
        }

        return ans;
    }
}
