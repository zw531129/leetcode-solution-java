package org.example.leetcode.interview.linkedlist;

import org.example.util.ListNode;

public class LC0021 {
    /**
     * 比较两个链表节点值的大小, 小的被curr链接, 最后处理剩余的一个节点
     * @param list1
     * @param list2
     * @return
     */
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode dummy = new ListNode(-1);
        ListNode curr = dummy;

        while (list1 != null && list2 != null) {
            if (list1.val < list2.val) {
                curr.next = list1;
                list1 = list1.next;
            } else {
                curr.next = list2;
                list2 = list2.next;
            }
            curr = curr.next;
        }
        if (list1 == null) {
            curr.next = list2;
        } else {
            curr.next = list1;
        }

        return dummy.next;
    }
}
