package org.example.leetcode.top150.arraystr;

import org.example.util.IO;

/**
 * @author zhangyf2595
 * @version 1.0
 * @date 2023/8/2
 **/
public class LC88 {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int i = nums1.length - 1;
        m--;
        n--;

        while (n >= 0) {
            while (m >= 0 && nums1[m] > nums2[n]) {
                swap(nums1, i--, nums1, m--);
            }
            swap(nums1, i--, nums2, n--);
        }
    }

    private void swap(int[] nums1, int i, int[] nums2, int j) {
        int tmp = nums1[i];
        nums1[i] = nums2[j];
        nums2[j] = tmp;
    }

    public static void main(String[] args) {
        LC88 solu = new LC88();
        solu.merge(new int[]{1, 2, 3, 0, 0, 0}, 3, new int[]{2, 5, 6}, 3);
        solu.merge(new int[]{1}, 1, new int[]{}, 0);
        solu.merge(new int[]{0}, 0, new int[]{1}, 1);
    }
}
